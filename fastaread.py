
def read_fasta_file_as_list_of_pairs(fileName):
    vector = []
    key = ''
    value = ''
    with open(fileName, 'r') as file:
        for line in file:
            if line[0] == '>':
                if key != '':
                    vector.append((key, value))
                key = line[1:].rstrip()
                value = ''  # aun no hemos leido ninguna secuencia
            else:
                value += line.rstrip()
    vector.append((key, value))
    file.close()
    return vector

def save_fasta_serialized(fileName):
    with open("serialized.txt", 'w') as output:
        list = read_fasta_file_as_list_of_pairs(fileName)

		for(pair1,pair2) in list:
			output.write(pair11 + ';' + pair2 + '\n')#dfsa