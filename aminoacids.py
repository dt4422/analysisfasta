import sys

from pyspark import SparkConf, SparkContext
import matplotlib.pyplot as plotter
from fastaread import save_fasta_serialized
if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Usage: spark-submit parallelize <file>", file=sys.stderr)
        exit(-1)

    save_fasta_serialized(sys.argv[1])
    sparkConf = SparkConf()
    sparkContext = SparkContext(conf=sparkConf)

    sparkContext.setLogLevel("OFF")



    resultados = []

    out = sparkContext \
        .textFile('serialized.txt') \
        .map(lambda line1: line1.split(':')) \
        .map(lambda line: (line[0], list(line[1]))) \
        .map(lambda x: (x[0], tuple(set((z, x[1].count(z)) for z in x[1])))) \
        .collect()
    nombres = []
    nombresChar = []
    tuplas = []
    for (word) in out:
        print(word)
        nombres.append(word[0])
        tuplas.append(word[1])

    output2 = sparkContext \
        .textFile('serialized.txt') \
        .map(lambda line: line.split(':')) \
        .map(lambda line: (line[0], len(line[1]))) \
        .collect()

    for (word, count) in output2:
        print("%s: %i" % (word, count))

    numeros=[]
    sparkContext.stop()
    nombrechar = []
    i=0
    for lista in tuplas:
         for nombre in lista:

             nombrechar.append(nombre[0])
             numeros.append(nombre[1])
         figureObject, axesObject = plotter.subplots()

         axesObject.pie(numeros,

                    labels=nombrechar,

                    autopct='%1.2f',

                    startangle=90)



         axesObject.axis('equal')
         nombrechar=[]
         numeros=[]
         plotter.title(nombres[i])
         i=i+1
        #Si quieres ver las imagenes todas juntas block=true
         plotter.show(block=True)







